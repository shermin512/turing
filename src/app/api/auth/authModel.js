import Decorator from '../../helpers/decorator';
import AuthDao from '../../database/auth/authDao';
import * as constants from '../../../utils/constants'

let _this;

class AuthModel extends Decorator {

    constructor() {
        super();
        _this = this;
    }

    registerCustomer(user) {
        return new Promise((resolve, reject) => {
            let {name, email, password} = user;

            try {
                AuthDao.createCustomer(name, email, password);
                return resolve({ "status": 200 });
            }
            catch (reason) {
                return reject(reason);
            }
        });
    }

    login(user) {
        return new Promise((resolve, reject) => {
            let {email, password} = user;

            try {
                AuthDao.getLoginInfo(email).then((result) => {
                    console.log(result[0][0].password);
                    if(result) {
                        if(result[0][0].password === password) {
                            return resolve({ "status": constants.SUCCESS });
                        } else {
                            return resolve({ "status": constants.UNAUTHORIZED });
                        }
                    }
                });
            }
            catch (reason) {
                return reject(reason);
            }
        });
    }
}

export default new AuthModel();