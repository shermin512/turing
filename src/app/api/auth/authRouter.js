import express from 'express';
import Decorator from '../../helpers/decorator';
import authModel from './authModel';
import * as util from '../../../utils/utility'
import * as constants from '../../../utils/constants'

class AuthRouter extends Decorator {

    /**
   * constructor
   */
  constructor() {
    super();
    this.router = express.Router();

    // Customer
    this.router.post('/cus/register', this.cusRegister);
    this.router.post('/cus/login', this.cusLogin);
    return this.router;
  }

  /**
   * Sign up a customer
   * @param {express.Request} req - request object
   * @param {express.Response} res - response object
   */
  cusRegister(req, res) {
    let body = req.body;
    authModel.registerCustomer(body).then(
      (result)=> res.status(constants.SUCCESS).json(result)
    ).catch((err)=> util.handleError(err, res));
  }

  /**
   * Login a customer
   * @param {express.Request} req - request object
   * @param {express.Response} res - response object
   */
  cusLogin(req, res) {
    let body = req.body;
    authModel.login(body).then(
      (result)=> res.status(constants.SUCCESS).json(result)
    ).catch((err)=> util.handleError(err, res));
  }

}

export default new AuthRouter();