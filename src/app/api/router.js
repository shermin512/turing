// Import core modules
import express from 'express';
import authRouter from './auth/authRouter';
import productsRouter from './products/productsRouter';

let router = new express.Router();
router.use('/auth', authRouter);
router.use('/products', productsRouter);

export default router;