import Decorator from '../../helpers/decorator';
import ProductsDao from '../../database/products/productsDao';
import * as constants from '../../../utils/constants'

let _this;

class ProductsModel extends Decorator {

    constructor() {
        super();
        _this = this;
    }

    getAllItems(productsPerPage, startItem) {
        return new Promise((resolve, reject) => {
            try {
                ProductsDao.getProducts(1000, productsPerPage, startItem).then((result) => {
                    let products = [];
                    if(result[0] && result[0].length > 0) {
                        result[0].forEach(element => {
                            products.push(this.buildProduct(element));
                        });
                    } 
                    return resolve({ "status": constants.SUCCESS,
                                    "productList":products});
                }).catch(reject);
            }
            catch (reason) {
                return reject(reason);
            }
        });
    }

    buildProduct(dbObject) {
        let product = {
            productId:dbObject.product_id,
            name:dbObject.name,
            description:dbObject.description,
            price:dbObject.price,
            discountedPrice: dbObject.discounted_price,
            thumbnail:dbObject.thumbnail
        };
        return product;
    }
}

export default new ProductsModel();