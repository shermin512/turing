import express from 'express';
import Decorator from '../../helpers/decorator';
import productsModel from './productsModel';
import * as util from '../../../utils/utility'
import * as constants from '../../../utils/constants'

class ProductsRouter extends Decorator {

    /**
   * constructor
   */
  constructor() {
    super();
    this.router = express.Router();

    // Customer
    this.router.get('/getAllProducts', this.getAllProducts);
    return this.router;
  }

  /**
   * Get All Productsr
   * @param {express.Request} req - request object
   * @param {express.Response} res - response object
   */
  getAllProducts(req, res) {
    let productsPerPage = req.query["productsPerPage"];
    let startItem = req.query["startItem"];
    productsModel.getAllItems(productsPerPage, startItem).then(
      (result)=> res.status(constants.SUCCESS).json(result)
    ).catch((err)=> util.handleError(err, res));
  }

}

export default new ProductsRouter();