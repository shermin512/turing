import * as mysql from 'mysql';
//import configs, { connectionLimit } from "../localConfigs/mysql.json";

const configs = {
  "connectionLimit": 50,
  "acquireTimeout": 60000,
  "host": "localhost",
  "port": "3306",
  "user": "root",
  "password": "",
  "database": "ecommerce",
  "charset": "UTF8_GENERAL_CI",
  "debug": false,
  "waitForConnections" : true,
  "queueLimit" : 15000
};
/**
 * DbManager Class
 */
class DbManager {

  /**
   * Constructor
   */
  constructor() {    
    this.connectionLimit = Math.floor(configs.connectionLimit);
    this.pool = mysql.createPool(configs);
  }

  /**
   * Get connection pool
   * @return {Pool|*} pool
   */
  getPool() {
    return this.pool;
  }

}

export default new DbManager();
