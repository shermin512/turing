import * as query from './auth.query';
import executor from '../executor';

export default class AuthDao {

    static createCustomer(name, email, password){
        return executor.execute(query.CREATE_CUSTOMER,[name, email, password]);
    }

    static getLoginInfo(email){
        return executor.execute(query.GET_LOGIN_INFO,[email]);
    }
}

