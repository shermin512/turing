import * as query from './products.query';
import executor from '../executor';

export default class ProductsDao {

    static getProducts(shortProductDescriptionLength, productsPerPage, startItem){
        return executor.execute(query.GET_ALL_PRODUCTS,
            [shortProductDescriptionLength, productsPerPage, startItem]);
    }
}