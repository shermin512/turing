import http from 'http';
import app from './app';

http.createServer(app).listen(3000, '127.0.0.1');

console.log('Server running at http://127.0.0.1:3000/');