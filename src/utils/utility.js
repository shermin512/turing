export const handleError = function (err, res, statusCode) {
    console.log(`${err.path} => ${err.message}`);
  
    res.status(err.statusCod).json({
      status: statusCode,
      message: err.customMessage || `Internal server error @  ${new Date().toDateString()}`
    });
  };